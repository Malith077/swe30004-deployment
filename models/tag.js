/**
 *  tag.js
 *  Schema definition for tag objects in the system.
 *  @author: Quang Huynh
 *  @created: 20/08/2018
 *  @modified: 20/08/2018
 */


const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const natural = require('natural');

/**
 * Tag schema. This represents a tag that is used to classify stories.
 */
const TagSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  // The Number of times this tag is used
  counter: {
    type: Number,
    default: 1
  },
  synonyms: {
    type: [String],
  },
}).pre('save', function (next) {
  // On save a tag, generates its synonyms using WordNet.
  const wordNet = new natural.WordNet();
  const synonyms = new Set();
  const callbackFn = function (results) {
    for (let res of results) {
      for (let syn of res.synonyms) {
        synonyms.add(syn);
      }
    }
    for (let syn of synonyms) {
      this.synonyms.push(syn);
    }
    next();
  };
  wordNet.lookup(this.name, callbackFn.bind(this));
});


module.exports = mongoose.model('Tag', TagSchema);