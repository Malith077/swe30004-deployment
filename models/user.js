/**
 *  user.js
 *  Schema definition for user objects in the system.
 *  @author: Zane, Tim
 *  @created: 24/04/2018
 *  @modified: 14/09/2018
 */


const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');


/**
 * User schema. This represents a LSOA user.
 */
const userSchema = new mongoose.Schema({

  // user details. Contains user name, gender, address and date of birth.
  details: {
    // Name includes first name and last name.
    name: {
      first: {
        type: String,
        required: [true, 'First name cannot be empty.']
      },

      last: {
        type: String,
        required: [true, 'Last name cannot be empty.']
      }
    },

    // User gender. Whether MALE/FEMALE/OTHER/UNKNOWN
    gender: {
      type: String,
      enum: [
        'MALE',
        'FEMALE',
        'OTHER',
        'UNDISCLOSED',
        'UNKNOWN'
      ],
      default: 'UNKNOWN'
    },

    // User address. Only contains suburb and country.
    address: {
      suburb: String,
      country: String
    },

    // User date of birth
    dob: Date,

    // User date of death
    dod: {
      type: Date,
      default: undefined
    }
  },

  // account details
  account: {
    // Handle/username of the account
    handle: {
      type: String
      // TODO: This type is expected to be required. Temporary once integration
      // completed
      // required: [true, 'User handle cannot be empty.']
    },
    // Account email address
    email: {
      type: String,
      required: [true, 'Email cannot be empty.']
    },

    // Account password
    password: {
      type: String,
      required: [true, 'Password cannot be empty.']
    },

    // Account avatar image
    avatar_url: {
      type: String,
    },

    // Account created date
    created: {
      type: Date,
      default: Date.now()
    },

    // Account last login
    lastlogin: {
      type: Date,
      default: Date.now()
    },

    // Account type. There are 3 main types:
    // ** FREE: free accounts which is able to see public story only. Could not
    // **       use main features such as creating stories, see related stories.
    // ** TRIAL: accounts in trial is able to use system functionality in a
    // **        certain amount of time
    // ** PAID: accounts which is paid and is able to use system functionality.
    accounttype: {
      type: String,
      enum: [
        'FREE',
        'TRIAL',
        'PAID'
      ],
      default: 'FREE'
    },

    // Trial information contains the trial start date and trial duration.
    trial: {
      started: Date,
      duration: Number
    }
  },

  // A collection of relatives to this account. Each of a relative account will
  // be asked if the user trust it.

  // extended details to be used as prompts for later
    // A favourite quote that inspires you
    quote: {
      type: String,
    },
    // Who are you?
    brief_me: {
      type: String,
    },
    // How Do you describe yourself?
    self_description: {
      type: String,
    },
    // Place of birth
    birth_place: {
      type: String,
      ref: 'City',
    },
    // Main places you have lived?
    other_places_lived: {
      type: String,
    },
    // What was your family like?
    brief_family: {
      type: String,
    },
    // Who raised you?
    guardians: {
      type: String,
    },
    // Brother, sisters?
    siblings: {
      type: String,
    },
    // Any notable fmily member?
    famous: {
      type: String,
    },
    // What is the very first thing you remember?
    first_memory: {
      type: String,
    },
    // Favourite or unforgettable memory?
    other_memories: {
      type: String,
    },
    // Where did you go to Primary School?
    primary_school: {
      type: String,
    },
    // Where did you go to High School?
    high_school: {
      type: String,
    },
    // A notable event from your schooling days?
    school_story: {
      type: String,
    },
    // Who were your best friends growing up?
    best_friends :{
      type: String,
    },
    // TAFE, University, apprenticeship?
    further_education: {
      type: String,
    },
    // Any ailments that affect/ed your life?
    health: {
      type: String,
    },
    // Favourite holidat destinations?
    holidays: {
      type: String,
    },
    // Where have you worked?
    places_of_work: {
      type: String,
    },
    // What were your favourite toys?
    fav_childhood_toy: {
      type: String,
    },
    // What were your favourite games?
    fav_childhood_game: {
      type: String,
    },
    // What was your favourite meal?
    fav_meal: {
      type: String,
    },
    // What was your least favourite meal?
    worst_meal: {
      type: String,
    },
    // Tell us about your pets
    fav_pets: {
      type: String,
    },
    // What have you published?
    publications: {
      type: String,
    },
    // Social media?
    social_media: {
      type: String,
    },
    // Geneological sites you use?
    ancestry_site: {
      type: String,
    },
    // Your Blog or website?
    website: {
      type: String,
    },
    // What stories are you going to tell?
    Stories_you_want_to_tell: {
      type: String,
    },




  relations: [
    {
      // Relative LSOA user id
      relUser: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      },

      // Whether this relative is trusted by user.
      trusted: {
        type: Boolean,
        default: false
      },

      // Relation type
      relation: {
        type: String,
        enum: [
          'PARENT',
          'CHILD',
          'PARTNER',
          'SIBLING',
          'FRIEND',
          'ACQUAINTANCE',
          'OTHER',
          'UNKNOWN'
        ],
        default: 'UNKNOWN'
      }
    }
  ],

  preferences: {
    profile: {
      displayDOB: {
        type: Boolean,
        default: true
      },

      displayInSearch: {
        type: Boolean,
        default: true
      },
    }
  }
}, {collection: 'users'});


// Virtual fullname property is a combination of first name and last name
userSchema
  .virtual('fullname')
  .get(function() {
    return this.details.name.first + ' ' + this.details.name.last;
  });

// Encrypt password
userSchema.methods.encryptPassword = function(password){
  return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

// Decrypt and check password
userSchema.methods.validPassword = function(password){
  return bcrypt.compareSync(password, this.account.password);
};

module.exports = mongoose.model('User', userSchema);
