/**
 *  city.js
 *  Schema definition for story objects in the system.
 *  @author: Quang Huynh
 *  @created: 24/04/2018
 *  @modified: 30/07/2018
 */


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * City schema. This represents a city used in Life Story Online application.
 */
const CitySchema = new Schema({
  geo_id: {
    type: String,
    required: true,
  },
  country_code: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  }
});

CitySchema.virtual('display_name').get(function () {
  return this.name + ', ' + this.country_code;
});

module.exports = mongoose.model('City', CitySchema);