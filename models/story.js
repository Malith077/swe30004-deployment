/**
 *  story.js
 *  Schema definition for story objects in the system.
 *  @author: Quang Huynh
 *  @created: 24/04/2018
 *  @modified: 20/08/2018
 */


const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;


const RELATED_STORY_DEFAULT_LIMIT = 3;

/**
 * Story schema. This represents a story created by user.
 */
const StorySchema = new Schema({
  // Privacy setting of the story. This field is used to restrict accesses to
  // the story. If none of those privacy setting is set, the story is visible to
  // public.
  privacy: {

    // If this flag is on, only author's trusted users are able to read the
    // story
    trusted_only: Boolean,

    // A list of references to users who is specifically granted access to the
    // story
    specified: [{
      id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      }
    }],

    // A list of references to users who are restricted from accessing the story
    excepts: [{
      id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      }
    }],
  },

  // Story title
  title: {
    type: String,
    required: true
  },

  // A range of date the story refers to. This is optional
  date: {
    // Start date of the story. Optional
    start_date: Date,
    // End date of the story. Optional
    end_date: Date
  },

  // Location where the story refers to. This nullable field if not empty stores
  // id of the city in our database
  location: {
    type: Schema.Types.ObjectId,
    ref: 'City',
  },

  // Participants who mentioned or involved to the story
  participants: [{
    // Optional. The participant is an active user only.
    id: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    // Name of the participant
    name: String
  }],

  // References to the user who created the story
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },

  // References to the type of event. A story may refer to many events
  tags: [{
    type: Schema.Types.ObjectId,
    ref: 'Tag',
  }],

  // A text content of the story. All medias or content that non-text will be
  // omitted
  content: String,

  // The text content of the story, formatted in HTML
  formatted_content: String,

  // The string presentation of the Delta object. This is needed that Quill
  // editor framework
  delta: String,
  medias: [{
    id: {
      type: Schema.Types.ObjectId,
      ref: 'Media'
    }
  }]
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

StorySchema.virtual('posted_at').get(function () {
  return moment(this.created_at).format('D MMM Y - h:mma');
});

StorySchema.methods.findRelatedStories = function (lm) {
  const limit = lm || RELATED_STORY_DEFAULT_LIMIT;
  const locationId = this.location ? this.location._id : null;
  return this.model('Story')
      .aggregate([
        {
          $match: {
            _id: {$ne: this._id},
            $or: [
              {tags: {$in: this.tags}},
              {location: {$eq: locationId}}
            ]
          }
        },
        {
          $project: {
            _id: 0,
            id: '$_id',
            title: 1,
            tags: 1,
            location: 1,
            tag_matches: {
              $size: {$setIntersection: ['$tags', this.tags]}
            },
            city_match: {
              $eq: ['$location', locationId]
            },
            created_at: 1
          }
        },
        {
          $sort: {
            tag_matches: -1,
            city_match: -1
          }
        },
        {
          $limit: limit
        }
      ])
};

module.exports = mongoose.model('Story', StorySchema);