/**
 * crawler.js crawlers stories from the website Medium.
 *
 * https://medium.com
 *
 * @author: Quang
 * @created_at: 27/08/2018
 */

const _ = require('lodash');
const cheerio = require('cheerio');
const fetch = require('node-fetch');
const fs = require('fs');

const MEDIUM_URI = 'https://medium.com';
const MEDIUM_TOPIC_URI = 'https://medium.com/topic/';
const OUTPUT_FILE = process.argv[2] || './nlp/medium-tag-training-set.json';

const topics = [
  'disability',
  'family',
  'pets',
  'parenting',
  'relationships',
  'self',
  'travel'
];

function crawl(url = '') {
  return fetch(url, {
    method: 'GET'
  }).then(function (res) {
    if (res.status !== 200) {
      return Promise.reject(res.statusText);
    }
    return res.text();
  });
}

// For each topic, crawls all story in the topic
const crawlPromises = _.map(topics, function (topic) {
  const topicUri = MEDIUM_TOPIC_URI + topic;
  // Crawl topics
  return crawl(topicUri)
      .then(function (body) {
        const $ = cheerio.load(body);
        const postUris = [];
        $('a[href^="/p/"]').each(function () {
          const href = _.truncate($(this).attr('href'), {
            separator: '?',
            omission: ''
          });
          const postUri = MEDIUM_URI + href;
          postUris.push(postUri);
        });
        const postPromises = _.chain(postUris)
            .uniq()
            .map(function (uri) {
              return crawl(uri);
            });
        return Promise.all(postPromises);
      })
      .then(function (posts) {
        return _.chain(posts)
            .map(function (post) {
              // Extract content and tags from posts
              const $ = cheerio.load(post);
              // Extract tags
              const tags = [];
              $('ul[class^="tags"] li').text(function (_, tag) {
                tags.push(tag.toLowerCase());
              });
              // Extract content
              const content = $('div[class^="section-inner"]').text();
              return {
                content: content,
                tags: tags
              };
            })
            .filter(function (post) {
              // Filters posts that has no tag
              return post.tags.length > 0;
            })
            .value();
      })
});

Promise.all(crawlPromises)
    .then(function (topics) {
      const data = JSON.stringify(_.flatten(topics));
      fs.writeFile(OUTPUT_FILE, data, function() {
        console.log(`Written data to ${OUTPUT_FILE}`);
      });
    })
    .catch(function (err) {
      console.error(err);
    });

