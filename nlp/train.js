/**
 * train.js produces a tag classifier for LSOA system. Given a story,
 * the tag classifier can suggest what category the story falls into.
 *
 * @author: Quang
 * @created_at: 27/08/2018
 */

const _ = require('lodash');
const fs = require('fs');

const Classifier = require('./classifier');
const MultiLabelNBClassifier = require('./multi_label_naive_bayes');

const INPUT_FILE = process.argv[2] || './nlp/training_set/medium-tag-training-set.json';
const OUTPUT_FILE = process.argv[3] || './nlp/trained_classifier/classifier.json';

const trainingSet = readDataset();
const classifier = new Classifier(MultiLabelNBClassifier);

console.log('Add documents...');
// Adding document and classification to the classifier
_.forEach(trainingSet, function(input) {
  const document = input.content;
  const labels = input.tags;
  classifier.addDocument(document, labels);
});

// Trains the classifier
classifier.train();

console.log('Train complete!');
classifier.saveAsync(OUTPUT_FILE).then(() => {
  console.log(`Saved the classifier to ${OUTPUT_FILE}`);
});


function readDataset() {
  const data = JSON.parse(fs.readFileSync(INPUT_FILE).toString());
  console.log(`Read training input from ${INPUT_FILE} ...`);
  return data;
}
