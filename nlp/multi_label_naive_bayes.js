/**
 * Multi-label Naive Bayes classification. The class wraps
 * erelsgl/limdu's multi-label binary classification.
 *
 * More details at https://github.com/erelsgl/limdu
 *
 * @author Quang Huynh
 * @created_at 02/09/2018
 * @updated_at 02/09/2018
 */
const _ = require('lodash');
const fs = require('fs');
const natural = require('natural');
const limdu = require('limdu');

natural.PorterStemmer.attach();

/**
 * Naive Bayes Classifier wrapper.
 * @param opts Naive Bayes options
 * @constructor
 */
const MultiLabelNBClassifier = function (opts) {
  const nv = limdu.classifiers.Bayesian.bind(0, opts);
  this.mlnbc = new limdu.classifiers.multilabel.BinaryRelevance({
    binaryClassifierType: nv
  });

  this.batch = [];

  const TfIdf = natural.TfIdf;
  this.tfidf = new TfIdf();
};

/**
 * Adds a training input which includes a document and a (or set) label
 * associated with the document.
 * @param {Array|String} document
 * @param {Array|String} labels
 */
MultiLabelNBClassifier.prototype.addDocument = function (document, labels) {
  if (typeof document === 'string') {
    document = document.tokenizeAndStem();
  }
  this.batch.push({
    document: document,
    labels: labels
  });
  this.tfidf.addDocument(document);
};

/**
 * Trains the classifier.
 */
MultiLabelNBClassifier.prototype.train = function () {
  const ctx = this;
  const batch = _.map(this.batch, function (data, id) {
    const input = {};
    _.forEach(ctx.tfidf.listTerms(id), function (item) {
      input[item.term] = item.tfidf;
    });
    return {
      input: input,
      output: data.labels
    }
  });
  ctx.mlnbc.trainBatch(batch);
  ctx.batch = [];
};

/**
 * Return label(s) associated with the document
 * @param document
 * @returns {Array|String|Object}
 */
MultiLabelNBClassifier.prototype.classify = function (document) {
  const ctx = this;
  const input = {};
  const tokens = document.tokenizeAndStem();
  ctx.tfidf.addDocument(tokens);
  _.forEach(ctx.tfidf.listTerms(ctx.tfidf.documents.length - 1), function (item) {
    input[item.term] = item.tfidf;
  });

  const classification = ctx.mlnbc.classify(input, 1, true);
  if (classification.classes.length === 0) {
    const negs = classification.explanation.negative;
    classification.classes = _
        .chain(negs)
        .pickBy(function (val) {
          return _.startsWith(val[0], '1: ');
        })
        .map(function (val, key) {
          const posScore = Number.parseFloat(_.trimStart(val[0], '1: '));
          const nevScore = Number.parseFloat(_.trimStart(val[1], '0: '));
          return {
            name: key,
            score: posScore - nevScore
          };
        })
        .orderBy(['score', 'name'], ['desc', 'asc'])
        .map('name')
        .value();
  }

  return classification.classes;
};

/**
 * Asynchronously saves the classifier.
 * @param filepath
 * @returns {Promise}
 */
MultiLabelNBClassifier.prototype.saveAsync = function (filepath) {
  const ctx = this;
  return new Promise(function (resolve, reject) {
    const data = {
      tfidf: ctx.tfidf,
      classifier: ctx.mlnbc
    };
    fs.writeFile(filepath, JSON.stringify(data), function (err) {
      if (err) reject(err);
      resolve();
    });
  });
};

/**
 * Asynchronously loads a classifier from file.
 * @param filepath
 * @return {Promise}
 */
MultiLabelNBClassifier.prototype.loadAsync = function (filepath) {
  const ctx = this;
  return new Promise(function (resolve, reject) {
    fs.readFile(filepath, function (err, data) {
      if (err) reject(err);
      const parsedData = JSON.parse(data.toString());
      const Tfidf = natural.TfIdf;
      ctx.tfidf = new Tfidf(parsedData.tfidf);
      ctx.mlnbc.fromJSON(parsedData.classifier);
      ctx.batch = [];

      resolve(ctx);
    });
  });
};

module.exports = MultiLabelNBClassifier;
