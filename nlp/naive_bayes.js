/**
 * Naive Bayes classification. The class wraps NaturalNode/Natural's bayes
 * classifier. More details at https://github.com/NaturalNode/natural.
 *
 * @author Quang Huynh
 * @created_at 02/09/2018
 * @updated_at 02/09/2018
 */
const natural = require('natural');
natural.PorterStemmer.attach();

/**
 * Naive Bayes Classifier wrapper.
 * @param stemmer natural's stemmer class
 * @param smoothing natural's smoothing class
 * @constructor
 */
const NaiveBayesClassifier = function (stemmer, smoothing) {
  this.nbc = new natural.BayesClassifier(stemmer, smoothing);
};

/**
 * Adds a training input which includes a document and a (or set) label
 * associated with the document.
 * @param {Array|String} document
 * @param {String} label
 */
NaiveBayesClassifier.prototype.addDocument = function (document, label) {
  if (typeof label !== 'string') return;
  this.nbc.addDocument(document, label);
};

/**
 * Trains the classifier.
 */
NaiveBayesClassifier.prototype.train = function () {
  this.nbc.train();
};

/**
 * Return label(s) associated with the document
 * @param document
 * @returns {Array|String|Object}
 */
NaiveBayesClassifier.prototype.classify = function (document) {
  const classification = this.nbc.classify(document);
  const tags = [];

  if (Array.isArray(classification)) {
    for (let cl of classification) {
      tags.push(cl.label);
    }
  } else {
    tags.push(classification.label);
  }

  return tags;
};

/**
 * Asynchronously saves the classifier.
 * @param filepath
 * @returns {Promise}
 */
NaiveBayesClassifier.prototype.saveAsync = function (filepath) {
  const ctx = this;
  return new Promise(function (resolve, reject) {
    ctx.nbc.save(filepath, function (err, classifier) {
      if (err) reject(err);
      resolve(classifier);
    });
  });
};

/**
 * Asynchronously loads a classifier from file.
 * @param filepath
 * @return {Promise}
 */
NaiveBayesClassifier.prototype.loadAsync = function (filepath) {
  const ctx = this;
  return new Promise(function (resolve, reject) {
    natural.BayesClassifier.load(filepath, null, function(err, classifier) {
      if (err) reject(err);
      ctx.nbc = classifier;
      resolve(ctx);
    });
  });
};

module.exports = NaiveBayesClassifier;
