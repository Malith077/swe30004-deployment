/**
 * Classifier.
 *
 * A wrapper that wraps classification algorithms used to train tags in LSOA
 * system.
 * @author Quang Huynh
 * @created_at 02/09/2018
 * @updated_at 02/09/2018
 */

/**
 * Classifier wrapper.
 * @param classifier
 * @constructor
 */
const Classifier = function(classifier) {
  this.classifier = new classifier();
};

/**
 * Adds a training input which includes a document and a (or set) label
 * associated with the document.
 * @param {Array|String} document
 * @param {Array|String} labels
 */
Classifier.prototype.addDocument = function(document, labels) {
  this.classifier.addDocument(document, labels);
};

/**
 * Trains the classifier.
 */
Classifier.prototype.train = function() {
  this.classifier.train();
};

/**
 * Return label(s) associated with the document
 * @param document
 * @returns {Array|String|Object}
 */
Classifier.prototype.classify = function(document) {
  return this.classifier.classify(document)
};

/**
 * Asynchronously saves the classifier.
 * @param {String} filepath
 * @returns {Promise}
 */
Classifier.prototype.saveAsync = function(filepath) {
  return this.classifier.saveAsync(filepath);
};

/**
 * Asynchronously loads a classifier from file.
 * @param {String} filepath
 * @return {Promise}
 */
Classifier.prototype.loadAsync = function(filepath) {
  const ctx = this;
  return this.classifier.loadAsync(filepath).then(function(classifier) {
    ctx.classifier = classifier;
    return classifier;
  });
};

module.exports = Classifier;
