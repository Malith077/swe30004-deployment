/**
 * settings.js
 * Settings endpoint. Handles user setting changes.
 * @author: Zane
 * @created: 01/09/2018
 * @modified: 30/09/2018
 */


const express = require('express');
const router = express.Router();

const {check, validationResult} = require('express-validator/check');
const csrf = require('csurf');

const csrfProtection = csrf({cookie: true});

const User = require('../models/user');

// Rendering the settings page
router.get('/', csrfProtection, isLoggedIn, async function (req, res) {
  try {
    const user = await User.findById(req.user._id).exec();

    res.render('settings', {
      auth: req.user,
      csrfToken: req.csrfToken(),
      errors: req.flash('error'),
      title: 'Settings',
      preferences: user.preferences,
    });
  } catch (err) {
    return res.status(500).send(err);
  }
});

// Update user password
router.post(
    '/password',
    [
      check('new_password')
          .isLength({min: 8}).withMessage('Password should contains at least 8 characters')
          .custom((value, {req}) => {
            if (value !== req.body.verify_password) {
              throw new Error('Password confirmation incorrect');
            }
            return value;
          }).withMessage('Password confirmation does not match'),
    ],
    isLoggedIn,
    csrfProtection,
    async function(req, res) {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        req.flash('error', errors.array());
        return res.redirect('/settings');
      }

      const user = await User.findById(req.user._id).exec();

      // Verifying previous password before change
      // Express-validator cannot perform Mongoose queries, so we will do it here instead.
      if (!user.validPassword(req.body.old_password)) {
        req.flash('error', {
          location: 'body',
          param: 'new_password',
          value: '',
          msg: 'Old password incorrect.'
        });
        res.redirect('/settings');
      }

      // Encrypt the new password
      newPassword = user.encryptPassword(req.body.new_password);

      //Update the user model
      User.findByIdAndUpdate({_id : req.user._id}, {
        'account.password': newPassword
      }, function(err, result) {
        if (err) {
          console.error(err);
        }
        res.redirect('/settings');
      });
    },
);

// Update user settings
router.post(
  '/',
  isLoggedIn,
  csrfProtection,
  async function (req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.flash('error', errors.array());
      return res.redirect('/settings');
    }

    const user = await User.findById(req.user._id).exec();

    // Set defaults
    let displayInSearch = false;
    let displayDOB = false;
    let gender = user.details.gender;

    // Get submitted values
    if (req.body.show_in_search) {
      displayInSearch = isChecked(req.body.show_in_search);
    }
    if (req.body.display_dob) {
      displayDOB = isChecked(req.body.display_dob);
    }
    if (req.body.gender != 'UNCHANGED') {
      gender = req.body.gender;
    }

    // Update the user model
    User.findByIdAndUpdate({_id : req.user._id}, {
      'preferences.profile.displayInSearch': displayInSearch,
      'preferences.profile.displayDOB': displayDOB,
      'details.gender': gender
    }, function(err, result) {
      if (err) {
        console.error(err);
      }
      res.redirect('/settings');
    });
  },
);

/**
 * Check whether the user is authenticated. If the user is authenticated,
 * continue the next middleware; otherwise, redirect to sign in page.
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/signin');
}

// Returns whether a checkbox value is 'checked'
function isChecked(value) {
  return (value === 'on');
}

module.exports = router;
