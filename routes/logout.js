/**
 * logout.js
 * Logout endpoint. Handle logout action.
 * @author: Malith, Quang
 * @created: 10/05/2018
 * @modified: 20/08/2018
 */
const express = require('express');
const router = express.Router();

/**
 * In this route, the session will be destroyed as the user logs off of the
 * system and send them back to the sign in page
 */

router.get('/', isLoggedIn, function(req, res, next){
    req.logout();
    req.session.destroy();
    res.redirect('/signin');
});

/**
 * Requires logged in middleware
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isLoggedIn(req, res, next){
  if(req.isAuthenticated()){
    return next();
  }
  res.redirect('/');
}

module.exports = router;

