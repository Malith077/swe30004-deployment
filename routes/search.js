/**
 * Search.js
 * Search endpoint. Handle requests to access search feature.
 * @author: Scott, Zane
 * @created: 31/08/2018
 * @modified: 30/09/2018
 */

const City = require('../models/city')
const Story = require('../models/story');
const Tag = require('../models/tag');
const User = require('../models/user');
const express = require('express');
const router = express.Router();

const MAX_RESULTS = 5;

/**
 * Function to map story results to bare essential information
 * @param stories The stories to map
 * @return A reduced stories object
 */
function reduceStories(stories) {
  return stories.map(story => {
    return {
      id: story._id,
      title: story.title,
      posted_at: story.posted_at,
      tags: story.tags || []
    }
  });
}

/**
 * Function to map query results to id only
 * @param res The array of results to map
 * @return A reduced results array (id only)
 */
function mapId(res) {
  return res.map(item => {
    return item.id;
  });
}

router.get('/more/:type/:s', async function (req, res) {
  try {
    // search lowercase only
    const searchTerm = req.params.s.toLowerCase();
    let stories = [];

    // Get a list of users we are able to search
    const searchableUsers = await getSearchableUsers();

    // *** Grab relevent stories that relate to this search term ***
    switch (req.params.type) {
      case 'title':
        // search by title
        stories = await Story.find({'title': new RegExp(searchTerm, 'i')})
            .where('author').in(searchableUsers)
            .populate('tags')
            .sort({'created_at': -1})
            .exec();
        break;
      case 'tag':
        //search by tag
        const tags = await Tag.find({'name': new RegExp(searchTerm, 'i')}).exec();
        stories = await Story.find({'tags': {$in: mapId(tags)}})
            .where('author').in(searchableUsers)
            .populate('tags')
            .sort({'created_at': -1})
            .exec();
        break;
      case 'city':
        //search by location
        const locations = await City.find({'name': new RegExp(searchTerm, 'i')}).exec();
        stories = await Story.find({'location': {$in: mapId(locations)}})
            .where('author').in(searchableUsers)
            .populate('tags')
            .sort({'created_at': -1})
            .exec();
        break;
      default:
        throw "Invalid URL detected";
    }
    // Render the view
    res.render('search/search-type', {
      auth: req.user,
      title: 'Search Results | ' + searchTerm,
      stories: stories,
      search: req.params.s, //original search term
      type: req.params.type
    });
  } catch (err) {
    // Something went wrong, return a server error
    return res.status(500).send(err);
  }
});

/**
 * GET route. Returns the view that displays search results.
 */
router.get('/:s', async function (req, res) {
  try {
    // search lowercase only
    const searchTerm = req.params.s.toLowerCase();

    // Get a list of users we are able to search
    const searchableUsers = await getSearchableUsers();

    // *** Grab all the stories that relate to this search term ***
    // search by title
    const storiesByTitle = await Story.find({'title': new RegExp(searchTerm, 'i')})
        .where('author').in(searchableUsers)
        .populate('tags')
        .sort({'created_at': -1})
        .limit(MAX_RESULTS)
        .exec();

    //search by tag
    //first get id's of tags that match
    const tags = await Tag.find({'name': new RegExp(searchTerm, 'i')}).exec();
    //then search stories model using array of relevant tags
    const storiesByTag = await Story.find({'tags': {$in: mapId(tags)}})
        .where('author').in(searchableUsers)
        .populate('tags')
        .sort({'created_at': -1})
        .limit(MAX_RESULTS)
        .exec();

    //search by location
    //first get ids of locations that match
    const locations = await City.find({'name': new RegExp(searchTerm, 'i')}).exec();
    // then search stories model for those tags.
    const storiesByCity = await Story.find({'location': {$in: mapId(locations)}})
        .where('author').in(searchableUsers)
        .populate('tags')
        .sort({'created_at': -1})
        .limit(MAX_RESULTS)
        .exec();

    // Render the view
    res.render('search/search-all', {
      auth: req.user,
      title: 'Search Results | ' + searchTerm,
      stories: {
        // Reduce stories to relevant information only
        title: reduceStories(storiesByTitle),
        tag: reduceStories(storiesByTag),
        city: reduceStories(storiesByCity),
      },
      search: req.params.s //original search term
    });
  } catch (err) {
    // Something went wrong, return a server error
    return res.status(500).send(err);
  }
});

// Returns a list of user IDs that are searchable
async function getSearchableUsers() {
  const userObjs = await User.find({'preferences.profile.displayInSearch': true})
      .select('_id')
      .exec();
  return userObjs.map(user => user._id);
}
module.exports = router;

