/**
 * questionnaire.js
 *
 * Handles the User Questionnaire which is used to record information the
 * user might like to use in future stories.
 *
 * @author: Tim, Malith
 * @created: 15/08/2018
 * @modified: 29/09/2018
**/

const express = require('express');
const router = express.Router();

const {check, validationResult} = require('express-validator/check');
const csrf = require('csurf');
const csrfProtection = csrf({cookie: true});
const User = require('../models/user');

// auth: req.user,
// Render Questionnaire
router.get('/', csrfProtection, isLoggedIn, function (req, res) {
  const dob = new Date(req.user.details.dob);
  const dd = ("0" + dob.getDate()).slice(-2);
  const mm = ("0" + (dob.getMonth() + 1)).slice(-2);
  const _dob = dob.getFullYear()+"-"+(mm)+"-"+(dd);
  res.render('questionnaire', {
    dob : _dob,
    auth : req.user,
    csrfToken: req.csrfToken(),
    errors: req.flash('errors'),
    title: 'About You',
  });
});


 // Update user settings
 router.post(
   '/',
   isLoggedIn,
   csrfProtection,
   (req, res, next) => {
    //  Populating questionnaire query
     const query = {
      'details.dob' : req.body.dob,
      birth_place : req.body.birth_place,
      guardians : req.body.guardians,
      siblings : req.body.siblings,
      first_memory : req.body.first_memory,
      fav_childhood_toy : req.body.fav_childhood_toy,
      fav_childhood_game : req.body.fav_childhood_game,
      fav_meal : req.body.fav_meal,
      worst_meal : req.body.worst_meal,
      fav_pets : req.body.fav_pets,
      primary_school : req.body.primary_school,
      high_school : req.body.high_school,
      school_story : req.body.school_story,
      best_friends : req.body.best_friends,
      further_education : req.body.further_education,
      other_places_lived : req.body.other_places_lived,
      holidays : req.body.holidays,
      places_of_work : req.body.places_of_work,
      famous : req.body.famous,
      other_memories : req.body.other_memories,
      health : req.body.health,
      self_description : req.body.self_description,
      brief_me : req.body.brief_me,
      quote : req.body.quote,
      publications : req.body.publications,
      social_media : req.body.social_media,
      website : req.body.website,
      ancestry_site : req.body.ancestry_site,
      Stories_you_want_to_tell : req.body.Stories_you_want_to_tell
     }
    //  Populating the database for the user
     User.findByIdAndUpdate({_id : req.user._id}, {$set : query}, function(err, result){
      const errors = validationResult(req);
      if(err) {
        req.flash('errors', err)
        res.redirect('/questionnaire');
      }
      if(!err) {
        res.redirect('/home');
      }
     });
   },
 );


/**
 * Check whether the user is authenticated. If the user is authenticated,
 * continue the next middleware; otherwise, redirect to sign in page.
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/signin');
}

module.exports = router;
