/**
 * story.js
 * Create new story view endpoint. Returns the document that render create
 * story page.
 * @author: Quang Huynh
 * @created: 24/04/2018
 * @modified: 20/08/2018
 */
const Story = require('../models/story');
const Tag = require('../models/tag');

const _ = require('lodash');
const csrf = require('csurf');
const express = require('express');
const moment = require('moment');
const router = express.Router();

const csrfProtection = csrf({cookie: true});

// GET create story view endpoint. Response a document to render the view.
router.get('/create', csrfProtection, isLoggedIn, function (req, res) {
  res.render('story/create', {
    auth: req.user,
    csrfToken: req.csrfToken(),
    isEdit: false,
    title: 'Create your story',
  });
});

// GET story view endpoint.
router.get('/edit/:id', csrfProtection, isLoggedIn, async function (req, res) {
  try {
    const story = await Story
        .findById(req.params.id)
        .populate('author')
        .populate('location')
        .populate('tags')
        .exec();
    res.render('story/create', {
      auth: req.user,
      csrfToken: req.csrfToken(),
      isEdit: true,
      story: story,
      title: 'Edit story | ' + story.title,
    });
  } catch (e) {
    res.status(500).send(e.toString());
  }
});

// GET create story view endpoint.
router.get('/:id', csrfProtection, async function (req, res) {
  try {
    const story = await Story
        .findById(req.params.id)
        .populate('author')
        .populate('location')
        .populate('tags')
        .exec();
    const relatedStories = await story
        .findRelatedStories()
        .exec()
        .then(function (stories) {
          const promises = [];

          _.each(stories, function (story) {
            _.assign(story, {
              posted_at: moment(story.created_at).format('D MMM Y - h:mma')
            });
            promises.push(Tag.populate(story, {path: 'tags'}));
          });

          return Promise.all(promises);
        });
    const isOwner = req.user && req.user.id === story.author.id;
    res.render('story/view', {
      auth: req.user,
      csrfToken: req.csrfToken(),
      isOwned: isOwner,
      story: story,
      relatedStories: relatedStories || [],
      title: story.title
    });
  } catch (e) {
    res.status(500).send(e.toString());
  }
});

/**
 * Check whether the user is authenticated. If the user is authenticated,
 * continue the next middleware; otherwise, redirect to sign in page.
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/signin');
}

module.exports = router;
