/**
 * help.js
 * Help view endpoint. Returns the help page.
 * @author: Tim
 * @created: 27/03/2018
 * @modified: 10/08/2018
 */

const express = require('express');
const router = express.Router();

// GET help view endpoint.
router.get('/', function(req, res) {
  return res.render('help', {
    auth: req.user,
    title: 'Help',
  });
});

module.exports = router;
