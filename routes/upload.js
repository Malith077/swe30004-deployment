/**
 * upload.js
 * Profile picture upload endpoint. Handle upload action.
 * @author: Malith
 * @created: 13/08/2018
 * @modified: 13/08/2018
 */


const express = require('express');
const router = express.Router();
const upload = require('../config/upload');
const User = require('../models/user');

router.post('/',isLoggedIn, function(req, res) {
    upload.upload(req, res, function(err) {
        if (err) {
            res.send(err);
        } else {
            if(!req.file) {
                res.redirect('/profile/' + req.user._id);
            } else {
                const update = {
                    'account.avatar_url' : `/image/${req.file.filename}`
                };
                User.findByIdAndUpdate(req.user._id, update, function(err) {
                    if (err) {
                        console.log(err);
                    }
                });
                res.redirect('/profile/' + req.user._id);
            }
        }
    });
});

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

module.exports = router;