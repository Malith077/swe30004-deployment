/**
 * home.js
 * Homepage view endpoint. Returns the document that render home page.
 * @author: Quang Huynh
 * @created: 27/03/2018
 * @modified: 10/08/2018
 */
const Story = require('../models/story');
const express = require('express');
const router = express.Router();

// GET homepage view endpoint. Response a document to render the view.
router.get('/', function(req, res) {
  // If user logged in, redirect them to home page. Otherwise show the index.
  if (req.user) {
    return res.redirect('/home');
  } else {
    return res.render('index', {title: 'Life Story Online'});
  }
});

module.exports = router;
