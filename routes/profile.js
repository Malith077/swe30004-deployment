/**
 * profile.js
 * User profile endpoint. Handle requests to access user profile.
 * @author: Zane, Quang
 * @created: 10/05/2018
 * @modified: 30/09/2018
 */


const Story = require('../models/story')
const User = require('../models/user');
const express = require('express');
const {param, validationResult} = require('express-validator/check');
const moment = require('moment');
const router = express.Router();

/**
 * GET route. Returns the view that displays user profile.
 * If the user who is authenticated is viewing the profile, they will have access to profile editing tools.
 */
router.get('/:id',
    [
      // Ensure that the user ID requested is a valid ID
      param('id')
          .isLength(24)
          .matches(/^[a-f0-9]+$/)
          .withMessage('Invalid user ID')
    ],
    async function (req, res) {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        req.flash('error', errors.array());
        return res.redirect(400, '/');
      }
      try {
        // Get the user, and ensure that they actually exist
        const user = await User.findById(req.params.id).exec();
        if (!user) {
          return res.redirect(404, '/');
        }

        // Grab all the stories that belong to this user
        const stories = await Story.find({'author': user._id})
            .populate('tags')
            .sort({'created_at': -1})
            .exec();

        // Reduce stories to relevant information only
        const userStories = stories.map(story => {
          return {
            id: story._id,
            title: story.title,
            posted_at: story.posted_at,
            tags: story.tags || [],
          }
        });

        // Does this user wish to display their age?
        // TODO: Get this setting from the stored user model.
        let userAge = {
          display: user.preferences.profile.displayDOB
        }

        if (userAge.display) {
          // Is the users DOB specified?
          userAge.birthYear = user.details.dob ?
              moment(user.details.dob).format('YYYY')
              : 'Unknown';
          // Has this user passed away, and if so, when?
          userAge.deathYear = user.details.dod ?
              moment(user.details.dod).format('YYYY')
              : 'Present';
        }

        // Construct the profile that we will pass to the renderer
        const userProfile = {
          name: {
            full: user.fullname,
            first: user.details.name.first
          },
          handle: user.account.handle,
          stories: userStories,
          avatar_url: user.account.avatar_url,
          age: userAge
        };

        // Does the currently logged in user own this?
        let owned = false;
        if (req.user) {
          //Convert ID objects to strings
          const loggedInID = req.user._id.toString();
          const contentAuthorID = user._id.toString();
          owned = (loggedInID === contentAuthorID);
        }

        // Render the view
        res.render('profile', {
          auth: req.user,
          title: 'User Profile | ' + userProfile.name.full,
          profile: userProfile,
          ownedByUser: owned
        });
      } catch (err) {
        // Something went wrong, return a server error
        return res.status(500).send(err);
      }
    }
);

module.exports = router;

