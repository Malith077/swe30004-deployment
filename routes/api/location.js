/**
 * location.js
 *
 * @author: Quang Huynh
 * @created: 24/04/2018
 * @modified: 28/05/2018
 */
const express = require('express');
const fs = require('fs');
const fuse = require('fuse.js');

const router = express.Router();
const citiesData = JSON.parse(fs.readFileSync('geo/cities.json'));

// Initialise fuse search
const fuseOptions = {
  shouldSort: true,
  tokenize: true,
  matchAllTokens: true,
  includeScore: true,
  threshold: 0.3,
  location: 0,
  distance: 50,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    "country_code",
    "name"
  ]
};
const fuseSearch = new fuse(citiesData, fuseOptions);

/**
 *
 * @param: id {ObjectId}
 * @path: /api/location/search
 */
router.get('/search', function (req, res) {
  if (!req.query.city) {
    return res.status(400).send('City parameter not found');
  }
  const matches = fuseSearch.search(req.query.city.trim());
  res.status(200).json(matches.map(match => {
    return match.item;
  }));
});

module.exports = router;
