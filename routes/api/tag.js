/**
 * tag.js
 *
 * @author: Quang Huynh
 * @created: 20/08/2018
 * @modified: 01/09/2018
 */


const Tag = require('../../models/tag');

const Classifier = require('../../nlp/classifier');
const MultiLabelNBClassifier = require('../../nlp/multi_label_naive_bayes');
const Transaction = require('mongoose-transactions');

const _ = require('lodash');
const express = require('express');
const fuse = require('fuse.js');


const TAG_CLASSIFIER = './nlp/trained_classifier/classifier.json';

const router = express.Router();
const classifier = new Classifier(MultiLabelNBClassifier);

// Initialise fuse search
const fuseOptions = {
  shouldSort: true,
  tokenize: true,
  matchAllTokens: true,
  includeScore: true,
  threshold: 0.3,
  location: 0,
  distance: 50,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    "name"
  ]
};

/**
 * Tag searching endpoint.
 * @path: /api/tag/search?tag=$INPUT
 */
router.get('/search', async function (req, res) {
  try {
    if (!req.query.tag) {
      return res.status(400).send('Tag parameter not found');
    }
    const tag = req.query.tag.trim();
    const filteredTags = req.query.filters || [];
    // Firstly, fuzzy searches all tag in the database to find all possible
    // matches to the input tag.
    const allTags = await Tag
        .find({
          'name': {
            $nin: filteredTags
          }
        })
        .exec();
    const fuseSearch = new fuse(allTags, fuseOptions);
    const matches = fuseSearch.search(tag).map(match => {
      return match.item;
    });
    if (matches.length === 0) {
      return res.status(200).json([]);
    }
    // Then, find synonyms of the CLOSEST MATCH to the input.
    const closestMatch = matches[0];
    const synonyms = await Tag
        .find({
          'name': {
            $in: closestMatch.synonyms
          },
          '_id': {
            $ne: closestMatch._id
          }
        })
        .sort({counter: 'desc'})
        .limit(5)
        .exec();

    res.status(200).json(matches.concat(synonyms));
  } catch (e) {
    res.status(500).json(e.toString());
  }
});

/**
 * Loads tag classifier from file and handle /api/tag/generate route.
 */
classifier.loadAsync(TAG_CLASSIFIER).then(function (classifier) {
  /**
   * Retrieves tags from the database that matches names in tagNames. For tag
   * names that do not exist in the database, creates new entry for that tag
   * names in database
   * @param tagNames
   * @returns {Promise<*>}
   */
  const tagsFromDatabase = async function (tagNames) {
    const transaction = new Transaction();
    try {
      const tags = await Tag
          .find({
            'name': {
              $in: tagNames
            }
          })
          .exec();

      const unregisteredTagNames =
          _.differenceWith(tagNames, tags, function (name, tag) {
            return name === tag.name;
          });

      if (unregisteredTagNames.length > 0) {

        _.forEach(unregisteredTagNames, function(tagName) {
          transaction.insert('Tag', {name: tagName, counter: 0});
        });

        const result = await transaction.run();
        return _.concat(tags, result);
      }

      return tags;
    } catch (e) {
      console.log(e);
      const rollbackObj = await transaction.rollback().catch(console.error);
      console.log(rollbackObj);

      return [];
    }
  };

  /**
   * Prepares a tag response given client request and tags from the classifier.
   * @param req
   * @param tags
   */
  const prepareTagResponse = async function (req, tags) {
    const limit = req.body.limit || (tags ? tags.length : 0);
    const tempTags =
        req.body.database ?
            await tagsFromDatabase(tags).catch(console.log.error) :
            tags;

    const res = {};
    if (limit > 0 && tempTags.length > limit) {
      res.tags = _.take(tempTags, limit);
    } else {
      res.tags = tempTags;
    }

    return res;
  };

  router.post('/generate', async function (req, res) {
    if (!req.body.document) {
      return res.status(400).json({
        error: 'Could not find the document.'
      });
    }
    const document = req.body.document;
    const tags = classifier.classify(document);

    const response = await prepareTagResponse(req, tags).catch(console.error);
    return res.status(200).json(response);
  });
});


module.exports = router;
