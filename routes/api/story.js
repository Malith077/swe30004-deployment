/**
 * story.js
 * Story API. Contains endpoints to query story object from our database.
 * @author: Quang Huynh
 * @created: 24/04/2018
 * @modified: 20/08/2018
 */
const City = require('../../models/city');
const Story = require('../../models/story');
const Tag = require('../../models/tag');
const Transaction = require('mongoose-transactions');

const {check, validationResult} = require('express-validator/check');
const _ = require('lodash');
const csrf = require('csurf');
const express = require('express');
const xss = require('xss');

const router = express.Router();
const csrfProtection = csrf({cookie: true});

/**
 * GET method. The endpoint returns a story with given id as parameter.
 * @param: id {ObjectId}
 * @path: /api/story/{id}
 */
router.get('/:id', (req, res) => {
  Story.findById(req.params.id)
      .exec((err, story) => {
        if (err) {
          res.status(500).send(err);
        } else {
          res.status(200).send(story);
        }
      });
});

/**
 * GET method. The endpoint returns a list of stories from the
 * database.
 * @path: /api/story
 */
router.get('/', (req, res) => {
  Story.find()
      .exec((err, stories) => {
        if (err) {
          res.status(500).send(err);
        } else {
          res.status(200).send(stories);
        }
      });
});

/**
 * POST method. The endpoint creates a story document in our database collection.
 * Story field's values could be found in the body of the request.
 * @path: /api/story
 */
router.post('/',
    csrfProtection,
    isLoggedIn,
    [
      check('content').not().isEmpty(),
      check('title').not().isEmpty(),
      check('formatted_content').not().isEmpty()
    ],
    async function (req, res) {
      // Validate post data
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        req.flash('error', errors.array());
        return res.status(400).json(errors.array());
      }
      // Create a transaction
      const transaction = new Transaction();
      try {
        const tags = parseTags(req.body.tags);
        // Get added tags ids
        const tagIds = await addNewTags(transaction, [], tags);
        // Creates new story
        const storyData = {
          author: req.user.id,
          content: req.body.content,
          delta: req.body.delta,
          formatted_content: xss(req.body.formatted_content),
          title: req.body.title,
          tags: tagIds,
        };
        if (req.body.location !== '') {
          const city = await City.findOne({'geo_id': req.body.location}).exec();
          storyData.location = city._id;
        }
        transaction.insert('Story', storyData);

        const result = await transaction.run();
        res.status(200).json(result[result.length - 1]);
      } catch (e) {
        console.log(e);
        const rollbackObj = await transaction.rollback().catch(console.error);
        res.status(500).json(rollbackObj);
      }
    });

/**
 * PUT method. The endpoint updates a story given its id as parameter.
 * @path: /api/story/{id}
 */
router.put('/:id',
    csrfProtection,
    isLoggedIn,
    [
      check('content').not().isEmpty(),
      check('title').not().isEmpty(),
      check('formatted_content').not().isEmpty()
    ],
    async function (req, res) {
      // Validate post data
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        req.flash('error', errors.array());
        return res.status(400).json(errors.array());
      }
      if (!req.params.id) res.status(400).json('Missing story id');
      const storyId = req.params.id;
      // Create a transaction
      const transaction = new Transaction();
      try {
        const currentStory =
            await Story.findById(storyId).populate('tags').exec();
        // Processes tags from the request
        const tags = parseTags(req.body.tags);
        const updatedTagsIds =
            await updateTags(transaction, currentStory.tags, tags);
        // Creates new story
        const storyData = {
          content: req.body.content,
          delta: req.body.delta,
          formatted_content: xss(req.body.formatted_content),
          title: req.body.title,
          tags: updatedTagsIds,
        };
        if (req.body.location !== '') {
          const city = await City.findOne({'geo_id': req.body.location}).exec();
          storyData.location = city._id;
        }
        transaction.update('Story', storyId, storyData);

        const result = await transaction.run();
        res.status(200).json(result[result.length - 1].toJSON());
      } catch (e) {
        console.log(e);
        const rollbackObj = await transaction.rollback().catch(console.error);
        res.status(500).json(rollbackObj);
      }
    });

/**
 * DELTE method. The endpoint deletes a story given its id as parameter.
 * @path: /api/story/{id}
 */
router.delete('/:id', csrfProtection, isLoggedIn, async function (req, res) {
  const transaction = new Transaction();
  try {
    const story = await Story.findById(req.params.id).exec();
    const storyTags = story.tags;
    // Decreases tag counters
    _.forEach(storyTags, function (tagId) {
      transaction.update('Tag', tagId, {$inc: {counter: -1}});
    });
    // Removes the story
    transaction.remove('Story', req.params.id);
    // Executes the transaction
    const result = await transaction.run();
    res.status(200).json(result[result.length - 1]);
  } catch (e) {
    const rollbackObj = await transaction.rollback().catch(console.error);
    res.status(500).json(rollbackObj);
  }
});

/**
 * Processed tag from user request.
 * @param {Array<string>} tags
 * @returns {Array<string>}
 */
function parseTags(tags = []) {
  const parsedTags = _.chain(tags)
      .filter(function(tag) {
        return typeof tag === 'string';
      })
      .compact()
      .map(function (tag) {
        return _.lowerCase(_.trim(tag));
      })
      .uniq()
      .value();
  return _.uniq(parsedTags);
}

/**
 * Update tags from a story.
 * @param {Transaction} transaction
 * @param {Array<Tag>}currentTags
 * @param {Array<string>} newTags
 * @returns {Promise<Array>}
 */
async function updateTags(transaction, currentTags = [], newTags = []) {
  // Removes tags
  const beingRemovedTagById = getBeingRemovedTags(currentTags, newTags, 'id');
  _.forEach(beingRemovedTagById, function (tagId) {
    transaction.update('Tag', tagId, {$inc: {counter: -1}});
  });
  const unchangedTagsById = getUnchangedTags(currentTags, newTags, 'id');
  const addedTags = await addNewTags(transaction, currentTags, newTags);

  return _.concat(unchangedTagsById, addedTags);
}

/**
 *
 * @param {Transaction} transaction
 * @param {Array<Tag>} currentTags
 * @param {Array<string>} newTags
 * @returns {Promise<Array>}
 */
async function addNewTags(transaction, currentTags = [], newTags = []) {
  const involvedTags =
      _.differenceWith(newTags, currentTags, function (nt, ct) {
        return nt === ct.name;
      });
  const existingTags = await Tag.find({name: {$in: involvedTags}}).exec();
  const existingTagsId = _.map(existingTags, 'id');
  // Adds tags that are already existed in the system. Results in increasing
  // their counters.
  _.forEach(existingTagsId, function (tagId) {
    transaction.update('Tag', tagId, {$inc: {counter: 1}});
  });
  const existingTagsName = _.map(existingTags, 'name');
  // Extracting newly created tags
  const newlyCreatedTagsNames = _.difference(involvedTags, existingTagsName);
  const newlyCreatedTagsIds = _.map(newlyCreatedTagsNames, function (tag) {
    return transaction.insert('Tag', {name: tag});
  });
  return _.concat(existingTagsId, newlyCreatedTagsIds);
}

/**
 * Get tags that going to be removed from the story.
 * @param {Array<Tag>} currentTags
 * @param {Array<string>} newTags
 * @param {string} [by]
 * @returns {Array}
 */
function getBeingRemovedTags(currentTags, newTags, by) {
  const beingRemovedTags =
      _.differenceWith(currentTags, newTags, function (ct, nt) {
        return ct.name === nt;
      });
  if (by) {
    return _.map(beingRemovedTags, by);
  }

  return beingRemovedTags;
}

/**
 * Get tags that unchanged from user editing request.
 * @param {Array<Tag>} currentTags
 * @param {Array<string>} newTags
 * @param {string} [by]
 * @returns {Array}
 */
function getUnchangedTags(currentTags, newTags, by) {
  const unchangedTags =
      _.intersectionWith(currentTags, newTags, function (ct, nt) {
        return ct.name === nt;
      });
  if (by) {
    return _.map(unchangedTags, by);
  }
  return unchangedTags;
}

// Check login status
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.status(401).send('UNAUTHORIZED');
}

module.exports = router;
