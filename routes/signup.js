/**
 * signin.js
 * Signup endpoint. Handle signup action.
 * @author: Malith, Quang
 * @created: 10/05/2018
 * @modified: 20/08/2018
 */


const express = require('express');
const router = express.Router();

const {check, validationResult} = require('express-validator/check');
const csrf = require('csurf');
const passport = require('passport');

const csrfProtection = csrf({cookie: true});

// Rendering the signup page
router.get('/', csrfProtection, function (req, res) {
  res.render('signup', {
    csrfToken: req.csrfToken(),
    errors: req.flash('error'),
    title: 'Sign up'
  });
});

// Create the user profile
router.post(
    '/',
    [
      check('firstname').not().isEmpty().withMessage('First name should not be empty.'),
      check('lastname').not().isEmpty().withMessage('Last name should not be empty'),
      check('email').isEmail().withMessage('Invalid email'),
      check('password')
          .isLength({min: 8}).withMessage('Password should contains at least 8 characters')
          .custom((value, {req}) => {
            if (value !== req.body.verifypassword) {
              throw new Error('Password confirmation does not match password');
            }
            return value;
          }).withMessage('Password confirmation does not match'),
    ],
    csrfProtection,
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        req.flash('error', errors.array());
        return res.redirect('/signup');
      }
      passport.authenticate('local.signup', {
        successRedirect: '/questionnaire',
        failureRedirect: '/signup',
        failureFlash: true
      })(req, res, next);
    },
);

module.exports = router;
