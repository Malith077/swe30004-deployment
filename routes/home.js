/**
 * home.js
 * Homepage view endpoint. Returns the document that render home page.
 * @author: Quang Huynh
 * @created: 27/03/2018
 * @modified: 10/08/2018
 */
const Story = require('../models/story');
const express = require('express');
const router = express.Router();

// GET homepage view endpoint. Response a document to render the view.
router.get('/', async function(req, res) {
  try {
    // TODO(quanghuynh): For the current implementation. We send all stories
    // in the database to the client for the display. However, the proper way to
    // do this is to implement resume token to paginate stories.
    const stories = await Story
        .find({})
        .populate('author')
        .populate('location')
        .populate('tags')
        .sort({'created_at': -1})
        .exec();
    res.render('home', {
      title: 'Life Stories Online' ,
      auth: req.user,
      stories: stories,
    });
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
