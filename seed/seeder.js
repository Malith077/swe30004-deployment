/**
 * seeder.js
 * Create seeds for the database.
 *
 * @author: Quang Huynh
 * @created: 16/07/2018
 * @modified: 24/07/2018
 */
const bcrypt = require('bcrypt-nodejs');
const dotenv = require('dotenv');
const fs = require('fs');
const seeder = require('mongoose-seed');

const citiesData = JSON.parse(fs.readFileSync('geo/cities.json'));

dotenv.config();

// Connect to MongoDB via Mongoose
seeder.connect(process.env.DB_URL, function () {

  // Load Mongoose models
  seeder.loadModels([
    'models/city.js',
    'models/story.js',
    'models/user.js',
    'models/tag.js',
  ]);

  // Clear specified collections
  seeder.clearModels(['City', 'Story', 'User', 'Tag'], function () {

    // Callback to populate DB once collections have been cleared
    seeder.populateModels(data, function () {
      seeder.disconnect();
    });

  });
});

// Data array containing seed data - documents organized by Model
const data = [
  {
    'model': 'User',
    'documents': [
      {
        details: {
          name: {
            first: 'admin',
            last: 'admin'
          },
        },
        account: {
          handle: 'admin',
          email: 'admin@lsoa.com',
          password: bcrypt.hashSync("admin", bcrypt.genSaltSync(5), null),
        },
      }
    ]
  },
  {
    'model': 'City',
    'documents': citiesData
  },
  {
    'model': 'Tag',
    'documents': [
      {
        name: 'anniversary',
        counter: 0,
      },
      {
        name: 'wedding',
        counter: 0,
      },
      {
        name: 'party',
        counter: 0,
      },
      {
        name: 'birthday',
        counter: 0,
      },
      {
        name: 'childhood',
        counter: 0,
      },
    ]
  }
];