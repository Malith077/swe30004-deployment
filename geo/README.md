# How to populate city data?
* Navigate to `/geo` directory

```commandline
cd geo
```

* Run command
```commandline
node process.js cities15000.txt
```

The city data will be populated as:
```json
{
  "id": "String",
  "country_code": "String",
  "name": "String"
}
```

under `cities.json` .