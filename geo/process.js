/**
 * process.js
 *
 * Processes the city data set to cities.json file. The data set could be found
 * in cities15000.txt. The cities15000.txt file is downloaded from
 * http://www.geonames.org/
 *
 * @author Quang
 * @create_at 31/07/2018
 */
const fs = require('fs');

const filename = process.argv[2];
if (!filename) {
  console.log('Filename not found!');
  process.exit(1);
}

const cities = fs.readFileSync(filename).toString().split("\n");
cities.pop();
const processedCities = cities.map((cityData) => {
  const data = cityData.split('\t');
  return {
    geo_id: data[0],
    name: data[1],
    country_code: data[8],
  }
});

fs.writeFileSync('cities.json', JSON.stringify(processedCities));