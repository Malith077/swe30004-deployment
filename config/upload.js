/**
*  upload.js
*  Handles uploading file, currently functions for only uploading profile image
*  creating new users in the database.
* 
*  @author: Malith
*  @created: 13/08/2018
*  @modified: 13/08/2018
*/
const multer = require('multer');
const path = require('path');

const maxFileSize = 1000000;
//Configeration of the Multer package
//---Location of the files saved
//---Name pattern of the each profile image
const storage = multer.diskStorage({
    destination: './public/image/',
    filename: function(req, filename, callback) {
        callback(null, filename.fieldname+ '-' + Date.now() + path.extname(filename.originalname));
    }
});

//Multer configerations (File size and the callback functions)
const upload = multer({
    storage : storage,
    limits: {fileSize : maxFileSize},
    fileFilter:function(req, filename, callback) {
        checkFileType(filename, callback);
    }
}).single('profileImage');


function checkFileType(filename, callback) {
    const fileTypes = /jpeg|jpg|png|gif/;
    const extname = fileTypes.test(path.extname(filename.originalname).toLowerCase());
    const mimeType = fileTypes.test(filename.mimetype);
    if (mimeType && extname) {
        return callback(null, true);
    } else {
        callback('Error : Images only (Accepted extensions : jpeg,jpg,png,gif)');
    }
};

module.exports = {
    upload
};