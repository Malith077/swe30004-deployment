/**
 *  passport.js
 *  Handles authentication of existing users and
 *  creating new users in the database.
 * 
 *  @author: Zane, Malith, Quang
 *  @created: 28/05/2018
 *  @modified: 29/09/2018
 */
const passport = require('passport'); //requiring the passport package
const User = require('../models/user'); //requiring the user model
const LocalStrategy = require('passport-local').Strategy; //requiring the passport local strategy

//Serializing the user
passport.serializeUser(function(user, done){
    done(null, user.id);
});


//de-serializing the user
passport.deserializeUser(function(id, done){
    User.findById(id, function(err, user){
        done(err, user);
    });
});

//implementation of the local strategy for the user signup
passport.use('local.signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
},function(req, email, password, done){
    User.findOne({'account.email' : email}, function(err, user){
        if(err){
            return done(err);// return back eny errors
        }

        if(user){ //return an error message if user is already has an account
            req.session.errors = 'Email is already in use!';
            return done(null, false, req.flash('error', 'Email is already in use!'));
        }

        //creating a new user 
        const newUser = new User();
        newUser.account.email = email;
        newUser.account.password = newUser.encryptPassword(password);
        newUser.details.name.first = req.body.firstname;
        newUser.details.name.last = req.body.lastname;
        newUser.birth_place = " "; 
        newUser.guardians = " ";
        newUser.siblings = " ";
        newUser.first_memory = " ";
        newUser.fav_childhood_toy = " ";
        newUser.fav_childhood_game = " ";
        newUser.fav_meal = " ";
        newUser.worst_meal = " ";
        newUser.fav_pets = " ";
        newUser.primary_school = " ";
        newUser.high_school = " ";
        newUser.school_story = " ";
        newUser.best_friends = " ";
        newUser.further_education = " ";
        newUser.other_places_lived = " ";
        newUser.holidays = " ";
        newUser.places_of_work = " ";
        newUser.famous = " ";
        newUser.other_memories = " ";
        newUser.health = " ";
        newUser.self_description = " ";
        newUser.brief_me = " ";
        newUser.quote = " ";
        newUser.publications = " ";
        newUser.social_media = " ";
        newUser.website = " ";
        newUser.ancestry_site = " ";
        newUser.Stories_you_want_to_tell = " ";

        newUser.save(function(err, result){
            if(err){
                return done(err);
            }
            return done(null, newUser);
        });
    })
}));

//implementation of the local stategy to signin
passport.use('local.signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
},function(req, email, password, done){
    User.findOne({'account.email' : email}, function(err, user){

        if(err){
            return done(err);
        }

        //if no username is entered
        if(!user){
            req.session.errors = 'User not found!';
            return done(null, false, {message: 'no user found'});
        }

        //if the password is incorrect
        if(!user.validPassword(password)){
            req.session.errors = 'Incorrect password!';
            return done(null, false, {message: 'Wrong password'});
        }

        return done(null, user);
    })
}));
