/**
 * search.js
 * The script contains logic for the search bar. It allows users to search a
 * story given the story id.
 * @author: Scott
 * @created: 27/05/2018
 * @updated: 08/09/2018
 */


/**
 * Entry point of the script. On script load, attaches the handler to search
 * button.
 * 
 * Redirects to new page for search results
 */
window.onload = () => {
  const form = document.getElementById('search-form');
  const searchBox = document.getElementById('search-field');
  form.onsubmit = () => {
    if (searchBox.value.length !== 0) {
      window.location.href = '/search/' + searchBox.value;
    }
    // stop further event propagation
    return false;
  }
}