/**
 * questionnair.js
 * 
 * @author : Malith
 * @created: 29/09/2018
 * @modified: 29/09/2018
 */
/**
 * Extracts data from the questionnair.jade 
 * Sending extracted questionnair data to /questionnair router in order to post to the database
 */

$(function(){
    $('#questionnaire').on('submit', function(event){
        event.preventDefault();
        const csrf = $('#csrf').val();
        const dob = $('#dob').val();
        const birth_place = $('#birth_place').val();
        const guardians = $('#guardians').val();
        const siblings = $('#siblings').val();
        const first_memory = $('#first_memory').val();
        const fav_childhood_toy = $('#fav_childhood_toy').val();
        const fav_childhood_game = $('#fav_childhood_game').val();
        const fav_meal = $('#fav_meal').val();
        const worst_meal = $('#worst_meal').val();
        const fav_pets = $('#fav_pets').val();
        const primary_school = $('#primary_school').val();
        const high_school = $('#high_school').val();
        const school_story = $('#school_story').val();
        const best_friends = $('#best_friends').val();
        const further_education = $('#further_education').val();
        const other_places_lived = $('#other_places_lived').val();
        const holidays = $('#holidays').val();
        const places_of_work = $('#places_of_work').val();
        const famous = $('#famous').val();
        const other_memories = $('#other_memories').val();
        const health = $('#health').val();
        const self_description = $('#self_description').val();
        const brief_me = $('#brief_me').val();
        const quote = $('#quote').val();
        const publications = $('#publications').val();
        const social_media = $('#social_media').val();
        const website = $('#website').val();
        const ancestry_site = $('#ancestry_site').val();
        const Stories_you_want_to_tell = $('#Stories_you_want_to_tell').val();
        data = {
            _csrf : csrf,
            dob : dob,
            birth_place : birth_place, 
            guardians : guardians,
            siblings : siblings,
            first_memory : first_memory,
            fav_childhood_toy : fav_childhood_toy,
            fav_childhood_game : fav_childhood_game,
            fav_meal : fav_meal,
            worst_meal : worst_meal,
            fav_pets : fav_pets,
            primary_school : primary_school,
            high_school : high_school,
            school_story : school_story,
            best_friends : best_friends,
            further_education : further_education,
            other_places_lived : other_places_lived,
            holidays : holidays,
            places_of_work : places_of_work,
            famous : famous,
            other_memories : other_memories,
            health : health,
            self_description : self_description,
            brief_me : brief_me,
            quote : quote,
            publications : publications,
            social_media : social_media,
            website : website,
            ancestry_site : ancestry_site,
            Stories_you_want_to_tell : Stories_you_want_to_tell
        }

        const xhr = $.ajax({
            type : 'POST',
            crossDomain : false,
            data : data,
            url : '/questionnaire'
        });

        xhr.done(function(response){
            $(location).attr('href', '/home');
        });
    });
});