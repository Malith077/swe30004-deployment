/**
 * home.js
 *
 * @author: Quang
 * @created: 27/05/2018
 * @updated: 08/09/2018
 */


function hideNavigateButtons() {
  const numStories = $('.carousel-inner').children().length;
  const prevBtn = $('#prev-btn');
  const nextBtn = $('#next-btn');

  prevBtn.addClass('disabled');
  if (numStories < 2) {
    nextBtn.addClass('disabled');
  }

  $('#storyCarousel').on('slide.bs.carousel', function(ev) {
    if (ev.to > 0) {
      prevBtn.removeClass('disabled');
    }
    if (ev.to < numStories - 1) {
      nextBtn.removeClass('disabled');
    }
    if (ev.to === 0) {
      prevBtn.addClass('disabled');
    } else if (ev.to === numStories - 1) {
      nextBtn.addClass('disabled');
    }
  });
}

function initReadMoreBtn() {
  $('#read-more').click(function() {
    const currSId = $('.carousel-item.active .story').attr('data-story-id');
    window.location.href = '/story/' + currSId;
  });
}

$(document).ready(function() {
  hideNavigateButtons();
  initReadMoreBtn();
});
