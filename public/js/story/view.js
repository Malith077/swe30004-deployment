/**
 * view.js
 * Client script that handles delete story action.
 *
 * @author: Quang
 * @created_at: 20/08/2018
 * @updated_at: 29/09/2018
 */

/**
 * Entry point of the script. Initalise the modal
 */
$(document).ready(() => {
  $('#delete-story-btn').click(deleteStory);
});

function deleteStory() {
  const token = $('meta[name="csrf-token"]').attr('content');
  const storyId = $('.story-content').attr('data-story-id');
  const xhr = $.ajax({
    url: '/api/story/' + storyId,
    headers: {
      'CSRF-Token': token
    },
    method: 'DELETE',
  });
  xhr.done(function (story) {
      window.location.href = '/profile/' + story.author;
    })
      .fail(() => {
        toastr.options = {
          'preventDuplicates': true,
          'positionClass': 'toast-top-right',
          'extendedTimeOut': 1000,
          'timeOut': 3000,
        };
        toastr.error('Error with deleting story');
      });
}
