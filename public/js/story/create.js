/**
 * story.js
 * A script that contains logic to handle interactions with users in Create New
 * Story page. It initialises the text editor and integrate speech-to-text
 * feature to the editor. The script also handles the submit action which create
 * a POST AJAX request to server's API.
 *
 * @author: Quang
 * @created: 24/04/2018
 * @modified: 15/09/2018
 */

const TAGS_SELECTOR = '#tag-buttons *[data-tag]';
const TAG_LIMIT = 3;

let RECORDING = false;
let LAST_INTERIM = '';

const fadedOutToastOpts = function () {
  return {
    'preventDuplicates': true,
    'positionClass': 'toast-top-right',
    'extendedTimeOut': 1000,
    'timeOut': 3000,
  };
};

const progressiveToastOpts = function () {
  return {
    'preventDuplicates': true,
    'positionClass': 'toast-top-center',
    'extendedTimeOut': 0,
    'timeOut': 0,
  };
};

const updateBtnText = function(el, value) {
  $(el).contents()
      .filter(function() {
        return this.nodeType === Node.TEXT_NODE;
      })
      .remove();
  $(el).append(value);
};

/**
 * Composes an AJAX request.
 * @param request
 * @returns {*|{readyState, getResponseHeader, getAllResponseHeaders, setRequestHeader, overrideMimeType, statusCode, abort}}
 */
function prepareAjaxRequest(request) {
  const token = $('meta[name="csrf-token"]').attr('content');
  return $.ajax({
    url: request.url,
    headers: {
      'CSRF-Token': token
    },
    crossDomain: false,
    method: request.method,
    data: request.data
  });
}

/**
 * Produces a tag button element.
 * @param tag
 * @return jQuery
 */
function produceTagBtn(tag) {
  const tagBadge = $('<span></span>')
      .text(tag.counter > 0 ? tag.counter : '')
      .addClass('badge badge-light');

  return $('<button></button>')
      .text(tag.name)
      .addClass('btn tag btn-sm')
      .attr('data-tag', tag.name)
      .append(tagBadge)
      .click(function () {
        this.remove();
      });
}

/**
 * Returns a list of tags that are chosen by users.
 * @returns {*|jQuery}
 */
function getAddedTags() {
  const tags = $(TAGS_SELECTOR)
      .map(function() {
        return $(this).attr('data-tag');
      })
      .get();
  return _.uniq(tags);
}

/**
 * Adds a tag to the story.
 * @param tag
 */
function addTag(tag) {
  if (!tag || !tag.name) return;

  const storyTag = $('#story-tag');
  const addedTags = getAddedTags();
  tag.name = tag.name.trim().toLowerCase();
  // Skip adding tags that already added.
  if (addedTags.indexOf(tag.name) !== -1) {
    storyTag.typeahead('val', '');
    return;
  }
  addedTags.push(tag.name);

  const tagBtn = produceTagBtn(tag);
  $('#tag-buttons').append(tagBtn);
  storyTag.typeahead('val', '');
}

/**
 * The method collects input data from the user and creates a story object.
 * @param quill {Quill} the text editor where story content composed.
 * @returns {Object} story object
 */
function getStory(quill) {
  const storyTitle = $('#story-title').val();
  const content = quill.getText();
  const delta = JSON.stringify(quill.getContents());

  const editorWindow = document.getElementById('editor')
      .getElementsByClassName('ql-editor')[0];
  const formattedContent = editorWindow.innerHTML.toString();

  return {
    title: storyTitle,
    content: content,
    delta: delta,
    formatted_content: formattedContent,
    location: $('#story-location').attr('data-geo-id') || '',
    tags: getAddedTags()
  }
}

/**
 * Initialise auto generate tag button. Upon click, the client send a AJAX
 * request to /api/tag/generate to request tag suggestion from the system.
 * @param quill
 */
function initGenerateTag(quill) {
  const btn = $('#generate-tag-btn');
  btn.click(function () {
    btn.attr('disabled', true);
    const request = prepareAjaxRequest({
      url: '/api/tag/generate',
      method: 'POST',
      data: {
        document: quill.getText(),
        limit: TAG_LIMIT,
        database: true
      }
    });

    toastr.options = progressiveToastOpts();
    toastr.warning('Generating tags ...');
    request
        .done(function (data) {
          toastr.clear();
          if (!data.tags || data.tags.length === 0) {
            toastr.options = fadedOutToastOpts();
            toastr.warning('We have no tag suggestion for this story');
            return;
          }

          for (let tag of data.tags) {
            addTag(tag);
          }
        })
        .fail(function (err) {
          console.error(err);
        })
        .always(function () {
          btn.attr('disabled', false);
        });
  });
}

/**
 * Initialises a quill editor. Quill API documents can be found at:
 * https://quilljs.com/docs/api
 * @returns {*|Quill}
 */
function initEditor() {

  return new Quill('#editor', {
    modules: {
      toolbar: '#toolbar'
    },
    theme: 'snow'
  });

}

/**
 * Attaches event handlers to buttons in the page.
 * @param quill editor object where story content fetched.
 */
function initButtons(quill) {
  const createBtn = $('#create-story-btn');
  const isEdit = $('#main-script').attr('data-is-edit') === 'true';
  const method = isEdit ? 'PUT' : 'POST';
  const storyId = isEdit ? createBtn.attr('data-story-id') : '';
  const url = isEdit ? `/api/story/${storyId}` : '/api/story';
  // On create button clicked, send an AJAX request to server.
  createBtn.click(function () {
    createBtn.attr('disabled', true);

    const data = getStory(quill);
    const requiredData = ['title', 'content'];
    for (let required of requiredData) {
      if (!data[required] || data[required] === '\n') {
        toastr.options = fadedOutToastOpts();
        toastr.error(`Story ${required} is required`);
        return;
      }
    }

    const xhr = prepareAjaxRequest({
      url: url,
      method: method,
      data: data,
    });

    toastr.options = progressiveToastOpts();
    toastr.warning('Saving story ...');
    xhr
        .done((story) => {
          window.location.href = '/story/' + story._id;
        })
        .fail(() => {
          toastr.clear();
          toastr.options = fadedOutToastOpts();
          toastr.error('Error with creating story');
          createBtn.attr('disabled', false);
        });
  });
}


/**
 * Initialises a SpeechRecognition object. The page is currently using Google
 * Chrome's built in speech to text library. Result of the speech to text
 * feature will be append to the editor directly.
 * @param editor {Quill}
 * @returns {webkitSpeechRecognition}
 */
function initRecognition(editor) {
  const recognition = new webkitSpeechRecognition();
  if (!recognition) {
    throw 'Speech Recognition is not support on your browser.';
  }
  recognition.continuous = true;
  recognition.interimResults = true;
  recognition.lang = 'en-US';

  recognition.onstart = function() {
    RECORDING = true;

    const mic = $('#voice-record');
    updateBtnText(mic.get(), 'Recording ...');
    mic.addClass('on');
  };

  recognition.onend = function() {
    RECORDING = false;

    const mic = $('#voice-record');
    updateBtnText(mic.get(), 'Record your voice');
    mic.removeClass('on');
  };

  recognition.onresult = function(ev) {
    let finalResult = '';
    let interimResult = '';
    for (let i = ev.resultIndex; i < ev.results.length; i++) {
      if (ev.results[i].isFinal) {
        finalResult += ev.results[i][0].transcript;
      } else {
        interimResult += ev.results[i][0].transcript;
      }
    }
    if (finalResult !== '') {
      editor.deleteText(editor.getLength() - LAST_INTERIM.length - 1, LAST_INTERIM.length);

      finalResult = editor.getLength() === 1 || finalResult[0] === ' ' ?
              finalResult :
              ' ' + finalResult;
      editor.insertText(editor.getLength() - 1, finalResult);
      LAST_INTERIM = '';
    } else if (interimResult !== '') {
      interimResult = editor.getLength() === 1 || interimResult[0] === ' ' ?
          interimResult:
          ' ' + interimResult;
      editor.deleteText(editor.getLength() - LAST_INTERIM.length - 1, LAST_INTERIM.length);
      editor.insertText(editor.getLength() - 1, interimResult);
      editor.formatText(
          editor.getLength() - interimResult.length - 1,
          interimResult.length,
          'color',
          'grey');
      LAST_INTERIM = interimResult;
    }

  };

  recognition.onerror = function(ev) {
    const errToMsg = function(err) {
      switch (err) {
        case 'no-speech':
          return 'No speech input. Please check your microphone';
        case 'audio-capture':
          return 'No microphone found';
        case 'not-allowed':
          return 'Microphone is not allowed to be used in this site';
        default:
          return '';
      }
    };
    const msg = errToMsg(ev.error);
    if (msg !== '') {
      toastr.clear();
      toastr.options = fadedOutToastOpts();
      toastr.error(msg);
    }
  };

  return recognition;
}


/**
 * Initialise location search engine. The search engine uses typeahead.js and
 * bloodhound to matches cities against user input. The location dataset
 * contains 2 parts: local data and remote data. The local data is hardcoded
 * with 5 Australian city, while remote data is fetch from /api/local/search
 * endpoint.
 */
function initializeCitySearchEngine() {
  // Initialise typeahead.js and bloodhound
  const citiesLocal = [
    {
      'geo_id': '2158177',
      'name': 'Melbourne',
      'country_code': 'AU'
    },
    {
      'geo_id': '2147714',
      'name': 'Sydney',
      'country_code': 'AU'
    },
    {
      'geo_id': '2063523',
      'name': 'Perth',
      'country_code': 'AU'
    },
    {
      'geo_id': '2174003',
      'name': 'Brisbane',
      'country_code': 'AU'
    },
    {
      'geo_id': '2078025',
      'name': 'Adelaide',
      'country_code': 'AU'
    },
  ];
  const locationSource = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: citiesLocal,
    remote: {
      url: '/api/location/search?city=%CITY',
      wildcard: '%CITY'
    },
    identify: function (response) {
      return response.geo_id;
    }
  });
  $('#story-location').typeahead(
      {
        minLength: 2,
        hint: false,
        highlight: true,
        classNames: {
          input: 'form-control form-control-sm',
          menu: 'search-menu',
          suggestion: 'search-suggestion'
        },
      },
      {
        name: 'cities',
        source: locationSource,
        display: function (item) {
          return item.name + ', ' + item.country_code;
        },
        templates: {
          notFound: function () {
            return '<div class="search-suggestion">No matching city found</div>'
          },
          pending: function () {
            return '<div class="search-suggestion">Loading...</div>'
          }
        }
      }
  ).on('typeahead:change', function () {
    const locationInput = $('#story-location');
    if (locationInput.attr('data-city') !== locationInput.val()) {
      locationInput.attr('data-geo-id', '');
      locationInput.attr('data-city', '');
    }
  }).on('typeahead:select', function (ev, suggestion) {
    const locationInput = $('#story-location');
    locationInput.attr('data-geo-id', suggestion.geo_id);
    locationInput.attr('data-city', suggestion.name + ', ' + suggestion.country_code);
  });
}

/**
 *
 */
function initializeTagSearchEngine() {
  // Initialise typeahead.js and bloodhound
  const tagSource = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '/api/tag/search?tag=',
      prepare: function (query, settings) {
        const searchInput = $('#story-tag').typeahead('val');
        const selectedTagIDs = $('#tag-buttons')
            .children()
            .map(function () {
              return $(this).attr('data-tag')
            })
            .get();
        let completeQuery = encodeURIComponent(searchInput);
        if (selectedTagIDs.length > 0) {
          for (let id of selectedTagIDs) {
            completeQuery += ('&filters[]=' + encodeURIComponent(id));
          }
        }
        settings.url += completeQuery;

        return settings;
      },
    },
    identify: function (response) {
      return response._id;
    }
  });
  const storyTag = $('#story-tag');


  // Search Input helper functions
  $('#add-tag-btn').click(() => {
    const val = storyTag.typeahead('val');
    if (val !== '') {
      addTag({name: val, counter: 0});
    }
  });

  // Config Tag Searching UI element.
  storyTag.typeahead(
      {
        minLength: 2,
        hint: false,
        highlight: true,
        classNames: {
          input: 'form-control form-control-sm',
          menu: 'search-menu',
          suggestion: 'search-suggestion'
        },
      },
      {
        name: 'tags',
        source: tagSource,
        display: function (item) {
          return item.name;
        },
        templates: {
          notFound: function () {
            return '<div class="search-suggestion">No matching tag found</div>'
          },
          pending: function () {
            return '<div class="search-suggestion">Loading...</div>'
          },
          suggestion: function (data) {
            return '<div class="search-suggestion">' + data.name +
                '<span class="float-right">' + data.counter + '</span>';
          }
        }
      }
  ).on('typeahead:select', function (ev, suggestion) {
    addTag(suggestion);
  }).keydown(function (ev) {
    // Handles CLEAR keydown. On clearing empty value, remove the most recent
    // tag.
    const searchValue = storyTag.typeahead('val');
    // keyCode: 8 - CLEAR.
    if (ev.keyCode === 8 && searchValue === '') {
      $('#tag-buttons').children().last().remove();
    }
    // Handles ENTER keydown. Extra handler for user-defined tags.
    const isOpen = storyTag.typeahead('isOpen');
    const val = storyTag.typeahead('val');
    if (ev.keyCode === 13 && !isOpen && val !== '') {
      addTag({name: val, counter: 0});
    }
  });
}

/**
 * Enable speech to text button. Hence, toggles speech to text function on
 * clicking.
 * @param recognition
 */
function enableSpeechToText(recognition) {
  $('#voice-record').click(function() {
    if (RECORDING) {
      recognition.stop();
    } else {
      recognition.start();
    }
    $(this).blur();
  });
}

/**
 * Disable speech to text button on browsers that does not support
 * webkitSpeechRecognition.
 */
function disableSpeechToText() {
  // TODO (quanghuynh): Need a more visual way to inform user that the speech
  // to text functionality to be disabled.
  const mic = $('#voice-record');
  mic.click(() => {
    toastr.options = fadedOutToastOpts();
    toastr.warning('Your browser does not support record feature');
  });
}

/**
 * Entry point of the script. Initialises editor and speech to text objects.
 * Attaches event handlers to buttons.
 */
$(document).ready(() => {
  $('[data-toggle="tooltip"]').tooltip();

  const quill = initEditor();
  try {
    const recognition = initRecognition(quill);
    enableSpeechToText(recognition);
  } catch (e) {
    disableSpeechToText();
  }

  $(TAGS_SELECTOR).each(function(_, el) {
    $(el).click(function () {
      this.remove();
    });
  });

  initGenerateTag(quill);
  initButtons(quill);
  initializeCitySearchEngine();
  initializeTagSearchEngine();
});
