/**
 * app.js
 * The entry of the application. This is where dependencies, middlewares, routes
 * and database declared.
 *
 * @todo: Move MongoDB settings to .env file
 * @author: Quang, Zane, Malith, Tim
 * @created: 27/03/2018
 * @modified: 30/09/2018
 */


const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const dotenv = require('dotenv');
const express = require('express');
const flash = require('connect-flash');
const logger = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');
const path = require('path');
const session = require('express-session');
const sassMiddleware = require('node-sass-middleware');

// API Routers
const storyAPIRouter = require('./routes/api/story');
const locationAPIRouter = require('./routes/api/location');
const tagAPIRouter = require('./routes/api/tag');
//requiring the passport local signin package
require('./config/passport');

// View routers
const storyRouter = require('./routes/story');
const indexRouter = require('./routes/index');
const homeRouter = require('./routes/home');
const logoutRouter = require('./routes/logout');
const profileRouter = require('./routes/profile');
const searchRouter = require('./routes/search');
const settingsRouter = require('./routes/settings');
const signupRouter = require('./routes/signup');
const signinRouter = require('./routes/signin');
const questionnaireRouter = require('./routes/questionnaire');
const helpRouter = require('./routes/help');
const uploadRouter = require('./routes/upload');

dotenv.config();
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use(session({secret:'lsoa', resave:false, saveUninitialized: false}));
app.use(flash());
//passport initialization - position is important (always after express-session initialization)
app.use(passport.initialize());
app.use(passport.session());


//**very important
//setting up a global variable to check the authentication of a user
app.use(function(req, res, next){
  res.locals.login = req.isAuthenticated();
  next();
});

app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  debug: true,
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));

app.use(express.static(path.join(__dirname, 'public')));

// Declare paths to JQuery and Bootstrap
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')));
app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')));
// Path to Libraries
app.use('/quill', express.static(path.join(__dirname, 'node_modules/quill/dist')));
app.use('/typeahead_js', express.static(path.join(__dirname, 'node_modules/typeahead.js/dist')));
app.use('/lodash', express.static(path.join(__dirname, 'node_modules/lodash')));

// Client routers
app.use('/', indexRouter);
app.use('/home', homeRouter);
/*
  /logout & /profile routes must remain following order in order to prevent
  conflicts while server is running
*/
app.use('/profile', profileRouter);
app.use('/logout', logoutRouter);

app.use('/search', searchRouter);
app.use('/settings', settingsRouter);
app.use('/story', storyRouter);
app.use('/signup', signupRouter);
app.use('/signin', signinRouter);
app.use('/questionnaire', questionnaireRouter);
app.use('/help', helpRouter);
app.use('/upload', uploadRouter);
// API routers
app.use('/api/story', storyAPIRouter);
app.use('/api/location', locationAPIRouter);
app.use('/api/tag', tagAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Mongoose connection setup
mongoose.connect(process.env.DB_URL);
// Get Mongoose to use global promise
mongoose.Promise = global.Promise;
mongoose.connection
    .once('open', () => console.log('Database connected!'))
    .on('error', (err) => console.error('MongoDB connection error:', err));


module.exports = app;
